Boid = {}
Boid.__index = Boid



function Boid:create(x, y)
    local boid = {}
    setmetatable(boid, Boid)
    boid.position = Vector:create(x, y)
    boid.velocity = Vector:create(math.random(-10, 10) / 10, math.random(-10, 10) / 10)
    boid.acceleration = Vector:create(0, 0)
    boid.r = 5
    boid.vertices = {0, - boid.r * 2, -boid.r, boid.r * 2, boid.r, 2 * boid.r}
    boid.maxSpeed = 4
    boid.maxForce = 0.1

    boid.r = 255
    boid.g = 0
    boid.b = 0
    boid.a = 255
    return boid
end

function Boid:update(boids)
    if isSep then
        local sep = self:separate(boids)
        -- sep:mul(4)
        self:applyForce(sep)
    end
    if isAlign then
        local al = self:align(boids)
        self:applyForce(al)
    end
    if isCoh then
        local ch = self:coh(boids)
        self:applyForce(ch)
    end
    
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)
    self.position:add(self.velocity)
    self.acceleration:mul(0)
    self:borders()
end

function Boid:colorise(boids)
    self.r = 100 + (count / (AMOUNT/100)) * 155
    self.g = 50 + (count / (AMOUNT/100)) * 205
    self.b = 155 + (count / (AMOUNT/100)) * 155
end

function Boid:applyForce(force)
    self.acceleration:add(force)
end

function Boid:seek(target)
    local desired = target - self.position
    desired:norm()
    desired:mul(self.maxSpeed)
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    return steer
end


function Boid:borders()
    if self.position.x < -self.r then
        self.position.x = width - self.r
    end
    if self.position.x > width + self.r then
        self.position.x = self.r
    end

    if self.position.y < -self.r then
        self.position.y = height - self.r
    end
    if self.position.y > height + self.r then
        self.position.y = self.r
    end
end



function Boid:draw(boids)
    --r, g, b, a = love.graphics.getColor()

    --love.graphics.setColor(1, 1, 1)
    local count = 0
    local separation = 40
    for i = 0, #boids do
        local other = boids[i]
        local d = self.position:distTo(other.position)
        if d > 0 and d < separation then
            count = count + 1
        end
    end
    --print(count)
    local arg = count ^ 0.15
    local arg2 = AMOUNT ^ 0.15
    self.r = 255 - (arg/arg2)*255
    self.g = 0
    self.b = (arg/arg2)*255
    love.graphics.setColor(self.r / 255, self.g / 255, self.b / 255, self.a / 255)
    local theta = self.velocity:heading() + math.pi / 2
    love.graphics.push()
    love.graphics.translate(self.position.x, self.position.y)
    love.graphics.rotate(theta)
    love.graphics.polygon("fill", self.vertices)
    love.graphics.pop()

    
    --print("draw " .. self.r .. " " .. self.g .. " " .. self.b)
end


function Boid:separate(boids)
    local separation = 25.
    local steer = Vector:create(0,0)
    local count = 0
    for i = 0, #boids do
        local other = boids[i]
        local d = self.position:distTo(other.position)

        if d > 0 and d < separation then
            local diff = self.position - other.position
            diff:norm()
            diff:div(d)
            steer:add(diff)
            count = count + 1
        end
    end
    --love.graphics.setColor(self.r, self.g, self.b, 255)
    if count > 0 then
        steer:div(count)
    end
    if steer:mag() > 0 then
        steer:norm()
        steer:mul(self.maxSpeed)
        steer:sub(self.velocity)
        steer:limit(self.maxForce)
    end
    return steer
end

function Boid:align(boids)
    local separation = 25.
    local steer = Vector:create(0,0)
    local count = 0
    for i = 0, #boids do
        local other = boids[i]
        local d = self.position:distTo(other.position)

        if d > 0 and d < separation then
            local diff = self.position - other.position
            steer:add(other.velocity)
            count = count + 1
        end
    end
    if count > 0 then
        steer:div(count)
        steer:limit(self.maxForce)
    end
    
    return steer
end

function Boid:coh(boids)
    local separation = 25.
    local steer = Vector:create(0,0)
    local count = 0
    for i = 0, #boids do
        local other = boids[i]
        local d = self.position:distTo(other.position)

        if d > 0 and d < separation then
            local diff = self.position - other.position
            steer:add(other.position)
            count = count + 1
        end
    end
    if count > 0 then
        steer:div(count)
        steer:limit(self.maxForce)
    end
    return self:seek(steer)
end